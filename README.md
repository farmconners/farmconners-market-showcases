# FarmConners Market Showcases

Welcome to the repository! Please find the market showcases documentation [here](https://farmconners-market-showcase.readthedocs.io/en/latest/).

<img align="left" src="docs/source/images/WF_coin_Coosun_credited.png"  width="500">


Currently, wind farms are operated with the primary control objective to maximise power generation. As the penetration of wind energy rises, this strategy may no longer be feasible, especially if government subsidies are reduced. In such a scenario, wind farm flow control (WFFC) can offer wind farm operators additional flexibility to maximise profits. For example, if electricity prices fall operators could curtail power generation in favour of load reduction strategies. To date, the lack of convincing evidence for the economic case for WFFC has prevented its widespread adoption.

The [FarmConners project](https://www.windfarmcontrol.info/) launches a set of showcases that researchers within the WFFC community can use to assess the positive impact of their control strategies. These showcases are based on the TotalControl Reference Wind Power Plant with weather simulation data and estimated electricity prices for both 2020 and 2030, provided courtesy of the DTU Balancing Tool Chain. With this data, researchers can evaluate the performance of their control algorithms using an internationally recognised tool.


<img align="left" src="docs/source/images/flag_yellow_high.jpg"  width="100">

The FarmConners project has received funding from the European Union's Horizon 2020 research and innovation programme under grant agreement No. 857844.
