.. _performance_indicators:

Performance Indicators
=======================

Operating the WF with WFFC is compared to nominal operation without WFFC. The nominal operation serves here as baseline to evaluate the benefit of WFFC in the showcases.

Per Showcase
--------------------------------------

Here is the summary of the performance indicators per :ref:`showcases` defined:

+------------------+---------------------------------------------------------------------------------------+
| Showcase set     | Performance Indicators                                                                |
+==================+=======================================================================================+
| High Prices      | income gain                                                                           |
+------------------+---------------------------------------------------------------------------------------+
| Low Prices       | income gain & alleviation of structural load as restriction                           |
|                  |                                                                                       |
|                  | (*i.e.* either load neutral or decrease in load components while increase in revenue) |
+------------------+---------------------------------------------------------------------------------------+
| TSO-driven       | alleviation of structural load as restriction & index tracking of reference from TSO  |
|                  |                                                                                       |
|                  | (*i.e.* decrease in load components compared to normal operation)                     |
|                  |                                                                                       | 
|                  | (*i.e.* 75% of the rated TC-RWP power)                                                |
+------------------+---------------------------------------------------------------------------------------+

For high prices, it is assumed that maximum power production will be favoured to generate the highest possible income. There, the maximisation of the revenue is prioritised so there is no restrictions on the loads specified.

Low prices provide incentives to additionally perform structural load alleviation. Target levels for load alleviation are introduced as restrictions for the low-price showcases. The income gain is again used as a measure to compare operational strategies that fulfil a particular load alleviation to be reported as an outcome of the market showcase participation.

In the TSO-driven case, the produced power is set by the operator command, and thus, the income is determined, *i.e.* no changes attributed to income gain/power production. Therefore, the measures for comparison will be the level of structural load alleviation with respect to the baseline and the extent to which the reference from the TSO is followed, since its non-compliance may lead to penalties.

The restriction of structural loads in the low prices and TSO-driven scenarios are based on the loads if the WF is operated without WFFC, *i.e.* loads should either be equal or lower than in nominal operation. It will depend on the models and tools used by the showcase participants which components can be considered. 

Evaluation indices
--------------------------------------

* The **income** metric is defined as the relative income gain achieved with respect to the nominal case without WFFC (*i.e.* normal operation).

.. math::
    \text{income gain} = \text{price [per MWh]} \cdot \Delta P \cdot \texttt{bin_hours} \ \ \ \ \ \ \ \text{with} \\
    \\ 
    \Delta P = \left({\sum_{i=1}^{n}P_i}\right)_\textrm{Op.=WFFC} - \ \ \ \ \ \ \ \left({\sum_{i=1}^{n}P_i}\right)_{\textrm{Op.=Normal}}\,

:math:`\texttt{bin_hours}` are the number of hourly samples in the binned scenarios, :math:`n` is the total number of turbines considered which is 32 for the :ref:`reference_wind_farm`,  :math:`P_i` is the power produced by turbine :math:`i`, and :math:`Op.=WFFC` and :math:`Op.=Normal` correspond to operation under WFFC and normal operation, respectively.


* The **load alleviation** metric is based on the normalised Damage Equivalent Loads (DEL) of relevant structural components in comparison with the nominal operation without WFFC, namely the flapwise blade root bending moment :math:`FlapM`, total shaft bending moment :math:`ShaftM`, and total tower bottom bending moment, :math:`TBBM`.

.. math::
	\Delta DEL_{\substack{FlapM \\ ShaftM \\ TBBM}} = 1 - \left(\frac{DEL_{\substack{FlapM \\ ShaftM \\ TBBM}_{{Op.=WFFC}}}}{DEL_{\substack{FlapM \\ ShaftM \\ TBBM}_{{Op.=Normal}}}}\right)_{i} \\
	\ \ \ \ \ \text{i = 1, n}


* For the **reference tracking performance indices**, two indexes are proposed for the total power of the WF with respect to the TSO power reference signal: normalised root-mean square error (NRMSE), and mean absolute error (MAE).

.. math::    
	\text{NRMSE} = \sqrt{\left(\frac{\sum_{i=1}^{n}P_i - P_{WF, refTSO}}{P_{WF, refTSO}}\right)^2} \\
    \\
	\text{MAE} = \bigg| \sum_{i=1}^{n}P_i-P_{WF, refTSO} \bigg|


:math:`P_{WF, refTSO}` is the wind farm level reference power mandataed by the TSO, which is 75% of the rated TC-RWP power for the TSO-driven FarmConners market showcase.
	
