.. _how_to_participate:

How to Participate
==================
Please fill in the registration form below. The registered participants will receive invitation to a cloud folder hosted at DTU Wind Energy to upload their results. To see which parameters to upload with their correct taxonomy, see :ref:`performance_indicators`. The results should be uploaded by 30 November 2021, see also :ref:`the home page <index>`. The WFFC model results for the defined test cases will be sent for publication in co-authorship with all the showcases participants. 

Note that the FarmConners Market Showcases provide a unique opportunity to evaluate the results of your WFFC strategy in volatile electricity markets together with the others in the community. It also allows further collaboration and overall advancement of the WFFC technology. 

Just :ref:`contact us <contact_us>` if you have questions.

.. raw:: html

    <embed>
    <script type="text/javascript" src="https://www.formlets.com/static/js/iframeResizer.min.js"></script>
    <iframe class="formlets-iframe" src="https://www.formlets.com/forms/iViqPrGSF82PHNGc/?iframe=true&nofocus=y" frameborder="0" width="100%"></iframe>
    <script type="text/javascript" src="https://www.formlets.com/static/js/iframe.js"></script>
