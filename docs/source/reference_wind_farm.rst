.. _reference_wind_farm:

Reference Wind Farm
==================

The showcases are defined for a reference wind farm at an exemplary offshore location. An offshore WF is used because the market influence may be more pronounced for larger wind farms such as those installed offshore.

.. figure:: images/Ref_WF_Locations_full.png
	:figwidth: 40%
	:align: right

	Layout of the Total Control Reference Wind Power Plant (TC-RWP) and location in the FarmConners Market Showcases.

The `TotalControl Reference Wind Power Plant (TC-RWP) <https://ec.europa.eu/research/participants/documents/downloadPublic?documentIds=080166e5bb2bdc76&appId=PPGMS>`_ with publicly available specifications was chosen as the reference WF. The TC-RWP consists of 32 10 MW wind turbines in a staggered layout.

The simulations assume that the TC-RWP is located approximately 20 km west of the Danish wind farm Horns Rev. Denmark is chosen as the reference location due to the already high share of wind power in the Danish power system. This ensures that changing wind conditions have a more realistic effect on the electricity price in the simulated :ref:`Scenarios`. Due to its location, the reference WF is part of the power system in West Denmark (referred to as *DK1* here).


.. _subset_reference_wind_farm:

Simplified farm layout
------------------------------------------

.. figure:: images/Ref_WF_Locations_subset.png
	:figwidth: 40%
	:align: right

	Subset of the TC-RWP.

computational limitations may restrict you to simulate the whole reference wind farm with 32 turbines. In this case, you may use the subset of the TC-RWP as indicated in the figure. Please indicate this also when :ref:`registering <how_to_participate>`.

If you would prefer a further simplified layout, please :ref:`contact us <contact_us>`.
