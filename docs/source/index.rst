.. FarmConners Market Showcases documentation master file, created by
   sphinx-quickstart on Fri Oct 16 10:06 2020.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.


.. _index:

Welcome to the FarmConners Market Showcases!
================================================================================

.. figure:: images/WF_coin_Coosun_credited.png  
	:figwidth: 50%
	:align: left

First, here is a :ref:`quick summary <in_short>` for those eager to get started!

Wind farms are currently operated with the primary control objective to maximise power generation. As the penetration of wind energy rises, this strategy may no longer be feasible, especially if government subsidies are reduced. In such a scenario, wind farm flow control (WFFC) can offer wind farm operators additional flexibility to maximise profits. For example, if electricity prices fall operators could curtail power generation in favour of load reduction strategies. To date, the lack of convincing evidence for the economic case for WFFC has prevented its widespread adoption.

The `FarmConners project <https://www.windfarmcontrol.info/>`_ launches a set of showcases that researchers within the WFFC community can use to assess the positive impact of their control strategies. These showcases are based on the TotalControl Reference Wind Power Plant with weather simulation data and estimated electricity prices for both 2020 and 2030, provided courtesy of the DTU Balancing Tool Chain. With this data, researchers can evaluate the performance of their control algorithms using an internationally recognised tool.

The showcases repository is located `here 
<https://gitlab.windenergy.dtu.dk/farmconners/farmconners-market-showcases>`_.

All showcases participants will be involved in the final publication, where the comparison of different WFFC strategies will be performed using the defined showcases. Instructions on how to participate are provided on :ref:`how_to_participate`. The deadline is November 2021.

This documentation is based on the paper "FarmConners market showcases for wind farm flow control" by Kölle et al. (2020) that was presented at the 19th Wind Integration Workshop and published in the workshop’s proceedings.

..  figure:: images/flag_yellow_high.jpg
	:figwidth: 100
	:align: left

The FarmConners project has received funding from the European Union's Horizon 2020 research and innovation programme under grant agreement No. 857844.


Content
=======
.. toctree::
   :maxdepth: 2

   in_short.rst
   introduction.rst
   reference_wind_farm.rst
   Scenarios.rst
   showcases.rst
   performance_indicators.rst
   how_to_participate.rst
   frequently_asked_questions.rst
   contact_us.rst

