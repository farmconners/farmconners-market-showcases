.. _in_short:

In Short
==================


.. _What:

What?
------------------------------------------
The FarmConners Market Showcases are basically:

* Datasets of electricity prices and inflow (wind speed and direction)
* To be used as input to wind farm control (WFC)
* When simulating a reference wind farm


.. _Why:

Why?
------------------------------------------
Some (of all the good) reasons why you should participate are:

* Relative evaluation of your tool/approach together with others
* Revenue estimation for your tool/approach which could be handy when applying for future projects
* Co-authorship of the resulting FarmConners paper
* Your own publication about details of implementation and results
* Collaboration with researchers at leading European institutions in the field of wind farm control
* Supporting the advancement of wind farm control
* It is fun



.. _How:

How?
------------------------------------------

1. Register :ref:`here <how_to_participate>`
2. Set up the :ref:`reference wind farm <reference_wind_farm>` (or the simplified version) in your simulation tool
3. Run a simulation without WFC using the showcases data as input (nominal case)
4. Run a simulation with WFC using the showcases data as input
5. Submit your results

At any time, :ref:`contact us <contact_us>` if you have questions.


.. _Data_format:

Data format
------------------------------------------

The data is provided in CSV files.

* Binned data for the :ref:`Market Showcases <showcases>` with 1. high electricity prices and 2. low electricity prices is organized like
    +---------------------------+-----------------------+-------------------+-------------+-------------+--------------+--------------+---------------------+---------------+---------------+----------------+
    | Wind direction bin center | Wind Speed bin center | WS HubHeight mean | WS 50m mean | WS 80m mean | WS 100m mean | WS 150m mean | Wdir HubHeight mean | Wdir 50m mean | Wdir 80m mean | Wdir 100m mean | 
    +---------------------------+-----------------------+-------------------+-------------+-------------+--------------+--------------+---------------------+---------------+---------------+----------------+

    +----------------+----------------------+----------------+----------------+-----------------+-----------------+-------------------+--------------+
    | Wdir 150m mean | Ustar HubHeight mean | Ustar 50m mean | Ustar 80m mean | Ustar 100m mean | Ustar 150m mean | TI HubHeight mean | DA 2020 mean |
    +----------------+----------------------+----------------+----------------+-----------------+-----------------+-------------------+--------------+

* Data for the TSO-driven :ref:`Market Showcase <showcases>` is organized like
    +---------------+--------+--------+---------+---------+----------------+----------+----------+-----------+-----------+-----------------+-----------+-----------+------------+------------+--------------+
    | WS HubHeight  | WS 50m | WS 80m | WS 100m | WS 150m | Wdir HubHeight | Wdir 50m | Wdir 80m | Wdir 100m | Wdir 150m | Ustar HubHeight | Ustar 50m | Ustar 80m | Ustar 100m | Ustar 150m | TI HubHeight |
    +---------------+--------+--------+---------+---------+----------------+----------+----------+-----------+-----------+-----------------+-----------+-----------+------------+------------+--------------+


.. _Example:

Inspiration?
------------------------------------------

Here are some examples of implementation/workflow for different tools.

+----------------------------------------------------------------------------+---------------------------------------------------------------------------------------+
| For power objectives                                                       | For power & load objectives                                                           |
+============================================================================+=======================================================================================+
| * Low Cost WF Setup                                                        | * Medium Fidelity/Cost WF Setup                                                       |
|       Steady-state tools (FLORIS, Fuga, etc.)                              |       FAST.Farm or similar                                                            |
+----------------------------------------------------------------------------+---------------------------------------------------------------------------------------+
| * Simulations for ‘bins’ & control settings for                            | * Simulations for chosen ‘bins’ & control settings for                                |
|       - Wind speed bin center :math:`\pm 1m/s`                             |        - Wind speed bin center :math:`\pm 1m/s`                                       |
|       - Wind direction center :math:`\pm 15^\circ`                         |        - Wind direction center :math:`\pm 15^\circ`                                   |
|       - Control settings per turbine                                       |        - Control settings per turbine                                                 |
|           * Steering: :math:`0^\circ`: :math:`-5^\circ`: :math:`-35^\circ` |             * Steering: :math:`0^\circ`: :math:`-5^\circ`: :math:`-35^\circ`          |
|           * Down-regulation: 5%: 5%: 30%                                   |             * Down-regulation: 0%, 5%: 5%: 30%                                        |
|           * Combinations!                                                  |             * 20-min runs                                                             |
|                                                                            | * Build a surrogate                                                                   |
+----------------------------------------------------------------------------+---------------------------------------------------------------------------------------+
| * Optimize for                                                             | * Optimize for                                                                        |
|       - High-price -> revenue increase                                     |        - High-price -> revenue increase                                               |
|                                                                            |        - Low-price -> load alleviation                                                |
|                                                                            |        - TSO-driven -> load alleviation                                               |
+----------------------------------------------------------------------------+---------------------------------------------------------------------------------------+
